using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonedaScript : MonoBehaviour
{
    private CircleCollider2D cc;

    void Start()
    {
        cc = GetComponent<CircleCollider2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (gameObject.CompareTag("bronce"))
            {
                collision.gameObject.GetComponent<JugardorScript>().Monedas(tag);
            }else if (gameObject.CompareTag("plata"))
            {
                collision.gameObject.GetComponent<JugardorScript>().Monedas(tag);
            }
            else if (gameObject.CompareTag("oro"))
            {
                collision.gameObject.GetComponent<JugardorScript>().Monedas(tag);
            }
            Destroy(gameObject);
        }
        
    }
}
