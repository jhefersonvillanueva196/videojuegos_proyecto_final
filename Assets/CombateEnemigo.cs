using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombateEnemigo : MonoBehaviour
{
    private Transform tf;
    private BoxCollider2D bc;

    private void Start()
    {
        tf = GetComponent<Transform>();
        bc = GetComponent<BoxCollider2D>();
    }

    private void FixedUpdate()
    {
        if (tf.localPosition.y <= -17)
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(gameObject.tag == "Enemigo")
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                if (collision.GetContact(0).normal.y <= -0.9)
                {
                    collision.gameObject.GetComponent<JugardorScript>().ataqueRebote();
                    Muerte();
                }
                else
                {
                    foreach (ContactPoint2D punto in collision.contacts)
                    {
                        if (punto.normal.x <= -0.9 || punto.normal.x >= 0.9) // Derecha
                        {
                            collision.gameObject.GetComponent<JugardorScript>().Da�o();
                            return;
                        }
                    }
                }

            }
        }else if(gameObject.tag == "Jefe")
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                if (collision.GetContact(0).normal.y <= -0.9)
                {
                    collision.gameObject.GetComponent<JugardorScript>().ataqueRebote();
                }
                else
                {
                    foreach (ContactPoint2D punto in collision.contacts)
                    {
                        if (punto.normal.x <= -0.9 || punto.normal.x >= 0.9) // Derecha
                        {
                            collision.gameObject.GetComponent<JugardorScript>().Da�o();
                            return;
                        }
                    }
                }
            }                
        }
        
    }

    private void Muerte()
    {
        bc.isTrigger = true;
        tf.localScale = new Vector3(tf.localScale.x, -1);
    }
}
