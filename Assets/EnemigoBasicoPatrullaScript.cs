using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoBasicoPatrullaScript : MonoBehaviour
{
    private Rigidbody2D rb;

    #region Movimiento

    [Header("Movimiento")] //Agregar cabezaras
    [SerializeField] private float velocidad;
    [SerializeField] private Transform Suelo;
    [SerializeField] private float distancia;
    [SerializeField] private bool movimientoDerecha;

    #endregion

    #region Animaciones

    private Animator animator;

    #endregion
    
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        RaycastHit2D informacionSuelo = Physics2D.Raycast(Suelo.position, Vector2.down, distancia);
        rb.velocity = new Vector2(velocidad, rb.velocity.y);
        if(informacionSuelo == false)
        {
            Girar();
        }
    }

    private void Girar()
    {
        movimientoDerecha = !movimientoDerecha;
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y + 180, 0);
        velocidad *= -1;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(Suelo.transform.position, Suelo.transform.position + Vector3.down);
    }
}
