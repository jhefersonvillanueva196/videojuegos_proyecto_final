using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoBasicoScript : MonoBehaviour
{

    private Rigidbody2D rb;

    #region Movimiento

    [Header("Movimiento")]
    private float movimientoHorizontal = 0f;
    [SerializeField] private float velocidadMovimiento;
    [Range(0, 0.3f)] [SerializeField] private float suavizadoMovimiento;
    private Vector3 velocidad = Vector3.zero;

    #endregion

    #region ReferenciaJugador

    [Header("Referencias Jugador")]
    [SerializeField] private GameObject jugador;
    [SerializeField] private float distanciaAltura;
    [SerializeField] private float distanciaPerseguir;
    private Vector3 posicion;

    #endregion

    #region Animaciones

    private Animator animator;

    #endregion

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        movimientoHorizontal = MirarJugador() * velocidadMovimiento;
        animator.SetFloat("Horizontal", Mathf.Abs(movimientoHorizontal));
    }

    private void FixedUpdate()
    {
        Mover(movimientoHorizontal * Time.fixedDeltaTime);
    }

    private void Mover(float mover)
    {
        Vector3 velocidadObjetivo = new Vector2(mover, rb.velocity.y);
        rb.velocity = Vector3.SmoothDamp(rb.velocity, velocidadObjetivo, ref velocidad, suavizadoMovimiento);
        if (mover > 0)
        {
            Girar(-1);
        }
        else if (mover < 0)
        {
            Girar(1);
        }        
    }

    private void Girar(int direccion)
    {
        Vector3 escala = transform.localScale; //Se crea un nuevo vector
        escala.x = direccion; //Se cambia la direccion en x
        transform.localScale = escala; //Se le asigna ese vector al objeto actual
    }

    private int MirarJugador()
    {
        int direccion = 0;
        if (jugador == null) return 0;

        Vector3 posicionJugador = jugador.transform.localPosition;
        Vector3 posicion = transform.localPosition;

        if (Mathf.Abs(posicionJugador.x - posicion.x) <= distanciaPerseguir && posicionJugador.x < posicion.x && Mathf.Abs(posicionJugador.y - posicion.y) <= distanciaAltura)
        {
            direccion = -1;
        }
        else if (Mathf.Abs(posicionJugador.x - posicion.x) <= distanciaPerseguir && posicionJugador.x > posicion.x && Mathf.Abs(posicionJugador.y - posicion.y) <= distanciaAltura)
        {
            direccion = 1;
        }
        else
        {
            direccion = 0;
        }

        return direccion;
    }

    
}
