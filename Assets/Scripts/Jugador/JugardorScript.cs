using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class JugardorScript : MonoBehaviour
{
    private Rigidbody2D rb;
    private Transform tf;
    private CapsuleCollider2D cc;
    private AudioScript asc;

    #region Movimiento

    [Header("Movimiento")] //Agregar cabezaras
    private float movimientoHorizontal = 0f;
    [SerializeField] private float velocidadMovimiento; //SerializeField permite mostrar en las configuraciones de Unity sin ser publicas
    [Range(0, 0.3f)] [SerializeField] private float suavizadoMovimiento; //Range agrega un limite con una barra deslizable, El suabizado es para dejar o quitar dezlisamientos del personaje al dejar de correr
    private Vector3 velocidad = Vector3.zero;
    private bool mirando = true; //Para girar al personaje

    #endregion

    #region Salto

    [Header("Salto")]//Agregar cabezaras
    [SerializeField] private float fuerzaSalto;
    [SerializeField] private LayerMask Suelo; //Layer o mascaras de acuerdo a suelo, obtaculo, etc
    [SerializeField] private Transform controladoSuelo; //Ayuda para detectar el contacto con el suelo
    [SerializeField] private Vector3 dimensionesCaja; //Tama�o de la caja para detectar el suelo
    [SerializeField] private bool enSuelo; //Verifica si esta en el suelo o no
    private bool salto = false;

    #endregion

    #region Animaciones

    private Animator animator;

    #endregion

    #region Ataque

    [Header("Ataque")]
    [SerializeField] private float fuerzaRebote;

    #endregion

    #region Da�o

    [Header("Da�o")]
    [SerializeField] private int vidas;
    [SerializeField] private Vector3 checkPoint;

    #endregion

    #region Monedas

    [Header("Moneda")]
    [SerializeField] private int bronce;
    [SerializeField] private int plata;
    [SerializeField] private int oro;
    [SerializeField] private int puntaje;

    #endregion

    #region UI

    [Header("UI")]
    [SerializeField] private Text datos;

    #endregion

    #region Interaccion

    [Header("Interaccion")]
    [SerializeField] private bool cajaCargada = false;
    private GameObject caja;
    private bool contactoCaja = false;
    private Vector3 posicion;
    private Vector3 orientacion;
    private int direccion;

    #endregion

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        tf = GetComponent<Transform>();
        cc = GetComponent<CapsuleCollider2D>();
        datos.GetComponent<Text>();
        animator = GetComponent<Animator>();
        datos.text = "";
        asc = FindObjectOfType<AudioScript>();
    }

    private void Update()
    {
        movimientoHorizontal = Input.GetAxisRaw("Horizontal") * velocidadMovimiento;

        animator.SetFloat("Horizontal", Mathf.Abs(movimientoHorizontal));

        animator.SetFloat("VelocidadY", rb.velocity.y);

        if (Input.GetButtonDown("Jump")) //Detecta si se preciona la tecla espacio
        {
            salto = true;
            asc.seleccionarAudio(1, 0.5f);
        }

        if (contactoCaja == true && cajaCargada == false)
        {
            if (Input.GetKeyDown(KeyCode.X))
            {
                cajaCargada = true;
            }
        }
        else if (contactoCaja == true && cajaCargada == true)
        {
            if (Input.GetKeyDown(KeyCode.X))
            {
                cajaCargada = false;
                dejarCajas();
            }
        }
        if(transform.localPosition.y <= -17)
        {
            Da�o();
        }

        if (Input.GetKeyDown(KeyCode.P) && Time.timeScale > 0)
        {
            Time.timeScale = 0;
        }
        else if(Input.GetKeyDown(KeyCode.P) && Time.timeScale == 0)
        {
            Time.timeScale = 1f;
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        datos.text = "Vidas: " + vidas + "\nPuntos: " + puntaje;
    }

    private void FixedUpdate()// Ayuda al funcionamiento en equipos rapidos o lentos
    {
        if (cajaCargada == true) cargarCajas();

        enSuelo = Physics2D.OverlapBox(controladoSuelo.position, dimensionesCaja, 0f, Suelo); //Verifica si esta o no en contacto con el suelo

        animator.SetBool("enSuelo", enSuelo);
        
        Mover(movimientoHorizontal * Time.fixedDeltaTime, salto);// Cuida que en equipos rapidos o lentos se mueva igual
        
        salto = false; //Se asigana como falso a salto para que no envie la orden de salto siempre

        
    }

    private void Mover(float mover, bool saltar)
    {
        Vector3 velocidadObjetivo = new Vector2(mover, rb.velocity.y); //Asigna una nueva velocidad
        rb.velocity = Vector3.SmoothDamp(rb.velocity, velocidadObjetivo, ref velocidad, suavizadoMovimiento);// Realiza el movimiento del personaje y lo Suaviza
        if (mover > 0 && !mirando)
        {
            Girar();
        }
        else if (mover < 0 && mirando)
        {
            Girar();
        }
        if(enSuelo && saltar)
        {
            enSuelo = false;
            rb.AddForce(new Vector2(0f, fuerzaSalto));
        }
    }

    private void Girar()
    {
        mirando = !mirando;
        Vector3 escala = transform.localScale; //Se crea un nuevo vector
        escala.x *= -1; //Se cambia la direccion en x
        transform.localScale = escala; //Se le asigna ese vector al objeto actual
    }

    private void OnDrawGizmos() //Dibuja la caja de colision con el suelo
    {
        Gizmos.color = Color.black;
        Gizmos.DrawWireCube(controladoSuelo.position, dimensionesCaja);
    }

    public void ataqueRebote()
    {
        rb.velocity = new Vector2(rb.velocity.x, fuerzaRebote);
        puntaje += 5;
        asc.seleccionarAudio(3, 0.8f);
    }

    public void Da�o()
    {
        if (cajaCargada == true)
        {
            dejarCajas();
            cajaCargada = false;
        }
        if (vidas <= 1) Muerte();
        else
        {
            tf.position = checkPoint;
            vidas -= 1;
        }
    }

    private void Muerte()
    {
        cc.isTrigger = true;
        tf.localScale = new Vector3(tf.localScale.x, -1);
        asc.seleccionarAudio(0, 0.8f);
        if (transform.localPosition.y <= -30)
        {
            SceneManager.LoadScene(0);
        }
    }

    public void CheckPoint(Vector3 coordenadas)
    {
        checkPoint = coordenadas;
    }

    public void Monedas(string tipo)
    {
        if(tipo == "bronce")
        {
            bronce += 1;
            puntaje += 1;
        }
        else if (tipo == "plata")
        {
            plata += 1;
            puntaje += 5;
        }
        else if (tipo == "oro")
        {
            oro += 1;
            puntaje += 10;
        }
        asc.seleccionarAudio(2, 0.8f);
    }

    private void cargarCajas()
    {
        Vector3 posicion = transform.localPosition;
        Rigidbody2D cajaRb = caja.GetComponent<Rigidbody2D>();
        cajaRb.freezeRotation = true;
        caja.transform.position = new Vector3(posicion.x, posicion.y + 0.901768f);
    }

    private void dejarCajas()
    {
        Vector3 posicion = transform.localPosition;
        orientacion = transform.localScale;
        Rigidbody2D cajaRb = caja.GetComponent<Rigidbody2D>();
        BoxCollider2D cajabc = caja.GetComponent<BoxCollider2D>();
        cajaRb.freezeRotation = false;
        cajaRb.bodyType = RigidbodyType2D.Dynamic;
        cajabc.isTrigger = true;
        if (orientacion.x < 0)
        {
            direccion = -1;
        }
        else
        {
            direccion = 1;
        }
        caja.transform.position = new Vector3(posicion.x + (0.901768f * direccion), posicion.y);
    }

    public void Victoria()
    {
        asc.seleccionarAudio(4, 0.5f);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("caja"))
        {
            contactoCaja = true;
            caja = collision.gameObject;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("caja"))
        {
            contactoCaja = false;
            caja = null;
        }
    }
}
