using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioScript : MonoBehaviour
{
    [SerializeField] private AudioClip[] audios;

    private AudioSource controlAudio;

    private void Awake()
    {
        controlAudio = GetComponent<AudioSource>();
    }

    public void seleccionarAudio(int indice,float volumen)
    {
        controlAudio.PlayOneShot(audios[indice], volumen);
    }
}
