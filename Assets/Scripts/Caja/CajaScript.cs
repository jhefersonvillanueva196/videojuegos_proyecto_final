using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CajaScript : MonoBehaviour
{
    private Rigidbody2D rb;
    private BoxCollider2D bc;
    private SpawnCajasScript scs;

    [SerializeField] private int duracion;
    
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        bc = GetComponent<BoxCollider2D>();
        scs = FindObjectOfType<SpawnCajasScript>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Jefe") && rb.bodyType == RigidbodyType2D.Dynamic)
        {
            if(SceneManager.GetActiveScene().buildIndex == 1)
            {
                collision.gameObject.GetComponent<Jefe1Script>().Da�o();
            }else if(SceneManager.GetActiveScene().buildIndex == 2)
            {
                collision.gameObject.GetComponent<Jefe2Script>().Da�o();
            }
            scs.hayCaja = false;
            Destroy(gameObject);
        }
        else if(collision.gameObject.CompareTag("Suelo") && rb.bodyType == RigidbodyType2D.Dynamic)
        {
            Destroy(gameObject);
        }else if(collision.gameObject.CompareTag("sueloJefe") && rb.bodyType == RigidbodyType2D.Dynamic)
        {
            scs.hayCaja = false;
            Destroy(gameObject);
        }
    }
}
