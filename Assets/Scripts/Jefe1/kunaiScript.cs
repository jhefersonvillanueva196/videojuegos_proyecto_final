using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class kunaiScript : MonoBehaviour
{
    private Rigidbody2D rb;

    #region Movimiento

    [Header("Movimiento")]
    [SerializeField] private int duracion = 5;

    #endregion

    #region Persecucion

    [Header("Persecucion")]
    [SerializeField] private bool hayJugador = true;
    public GameObject jugador;
    private float rotacionZ;
    private float direccionX;
    private float direccionY;

    #endregion

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Destroy(gameObject, duracion);
    }

    private void Update()
    {
        if (jugador == null) hayJugador = false;
    }

    private void FixedUpdate()
    {
        if (hayJugador) Perseguir();
    }

    public void Perseguir()
    {
        direccionX = jugador.transform.position.x - gameObject.transform.position.x;
        direccionY = jugador.transform.position.y - gameObject.transform.position.y;
        rb.velocity = new Vector3(direccionX, direccionY);
        rotacionZ = Mathf.Abs(Mathf.Atan(direccionX / direccionY) * 180/Mathf.PI);
        if(direccionX > 0 && direccionY > 0) // primer cuadrante
        {
            rotacionZ *= -1;
        }
        else if (direccionX < 0 && direccionY < 0) // tercero cuadrante
        {
            rotacionZ = (rotacionZ + 180) * (- 1);
        }
        else if (direccionX > 0 && direccionY < 0) // cuarto cuadrante
        {
            rotacionZ = (rotacionZ + 180);
        }
        transform.eulerAngles = new Vector3(0, 0, rotacionZ);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<JugardorScript>().Da�o();
            Destroy(gameObject);
        }
    }

}
