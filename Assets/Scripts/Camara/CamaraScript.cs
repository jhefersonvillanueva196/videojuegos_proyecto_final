using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CamaraScript : MonoBehaviour
{

    private CinemachineVirtualCamera cmvc;

    [Header("Pelea Boss")]
    [SerializeField] private Transform jugador;

    private void Start()
    {
        cmvc = GetComponent<CinemachineVirtualCamera>();
    }

    void Update()
    {
        if(jugador.position.x >= 114.82f && jugador.position.x <= 142.89f)
        {
            cmvc.Follow = null;
            cmvc.m_Lens.OrthographicSize = 8;
            transform.localPosition = new Vector3(127.42f, -3.65f, -10);
        }
        else
        {
            cmvc.Follow = jugador;
            cmvc.m_Lens.OrthographicSize = 6;
        }
    }
}
