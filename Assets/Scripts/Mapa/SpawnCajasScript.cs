using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCajasScript : MonoBehaviour
{
    [Header("Cajas spawn")]
    [SerializeField] private Transform spawn;
    [SerializeField] private GameObject cajaPrefrab;
    [SerializeField] public bool hayCaja = false;

    private void Update()
    {
        if(hayCaja == false)
        {
            var x = spawn.position.x;
            var y = spawn.position.y;
            var caja = Instantiate(cajaPrefrab, new Vector2(x, y), Quaternion.identity);
            hayCaja = true;
        } 
    }
}
