using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PuertasScript : MonoBehaviour
{
    private CompositeCollider2D cc;
    private Transform tf;

    [Header("Requisitos")]
    [SerializeField] private GameObject jugador;
    [SerializeField] private GameObject jefe;
    private bool vivo;
    private int escena;

    private void Start()
    {
        cc = GetComponent<CompositeCollider2D>();
        tf = GetComponent<Transform>();
    }

    private void Update()
    {
        escena = SceneManager.GetActiveScene().buildIndex;
        if(jefe != null)
        {
            if (escena == 1) vivo = jefe.GetComponent<Jefe1Script>().vivo; else if (escena == 2) vivo = jefe.GetComponent<Jefe2Script>().vivo;
            if (vivo == true && jugador.transform.position.x < 112.82f)
            {
                tf.localScale = new Vector3(0, 0, 0);
            }
            else if (vivo == true && jugador.transform.position.x >= 114.82f)
            {
                tf.localScale = new Vector3(1, 1, 0);
            }
            else if (vivo == false)
            {
                tf.localScale = new Vector3(0, 0, 0);
            }
        }
    }
}
