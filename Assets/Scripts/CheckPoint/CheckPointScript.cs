using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointScript : MonoBehaviour
{

    #region CheckPoint

    [Header("CheckPoint")]
    [SerializeField] private bool subida = false;

    #endregion


    #region Animaciones

    private Animator animator;

    #endregion

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            subida = true;
            animator.SetBool("Activa", subida);
            collision.gameObject.GetComponent<JugardorScript>().CheckPoint(transform.localPosition);
        }
    }
}
